//
//  ViewController.swift
//  TwitterFabric
//
//  Created by Illya on 4/24/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import UIKit
import TwitterKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logInButton = TWTRLogInButton { (session, error) in
            if let unwrappedSession = session {
                let alert = UIAlertController(title: "Logged In",
                                              message: "User \(unwrappedSession.userName) has logged in",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                NSLog("Login error: %@", error!.localizedDescription);
            }
        }
        
        // TODO: Change where the log in button is positioned in your view
        if let button = logInButton {
            button.center = self.view.center
            self.view.addSubview(button)
        }


//        let twitterLogInButton = TWTRLogInButton { (TWTRSession?, Error?) in
//            
//            ///
//        }
        
//        twitterLogInButton.center = view.center
//        view.addSubview(twitterLogInButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

